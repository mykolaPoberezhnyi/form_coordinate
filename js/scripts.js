$(document).ready(function() {
    $('body').on('click', '.add_new_point', function(e) {
        e.preventDefault()
        const parentEl = parent($(this))

        addRow();
    })
    $('body').on('click', '.remove_point', function(e) {
        e.preventDefault()
        const parentEl = parent($(this))
        parentEl.remove()
    })
    $('body').on('submit', '#coordinate_form', function(e) {
        e.preventDefault();
        const coordinate_data = form_to_json($(this))

        let form_data = [
            {
                "name": $(this).find('input[name=name]').val()
            },
            {
                "surname": $(this).find('input[name=surname]').val()
            },
            {
                "father": $(this).find('input[name=father]').val()
            },
            {
                "date": $(this).find('input[name=date]').val()
            },
            {
                "count": $(this).find('input[name=date]').val()
            },
            {
                "coordinates": coordinate_data
            }
        ]
        send_form('/', form_data)
        console.log(form_data)
    })


    function addRow(elem, n) {
        $(`<div class="row justify-content-center">
            <div class="col-sm-1 col-md-1 d-flex align-items-end">
                <div class="form-group">
                    <button type="button" class="btn btn-light remove_point"><i class="far fa-window-close"></i></button>
                </div>
            </div>
            <div class="col-sm-5 col-md-3">
                <div class="form-group">
                    <label>Координата X</label>
                    <input type="text" class="form-control" name="coordinate_x" placeholder="Введіть координату X">
                </div>
            </div>
            <div class="col-sm-5 col-md-3">
                <div class="form-group">
                    <label>Координата Y</label>
                    <input type="text" class="form-control" name="coordinate_y" placeholder="Введіть координату Y">
                </div>
            </div>
        </div>`).insertBefore('#add_new_point_row')
    }	
    function parent(elem) {
        return elem.closest('.row')
    }
    function form_to_json(form) {
        const x = form.find('input[name=coordinate_x]')
        const y = form.find('input[name=coordinate_y]')
        let formJSON = []
        for (let i = 0; i < x.length; i++) {
            formJSON.push({
                "coordinate_x": x[i].value.toString(),
                "coordinate_y": y[i].value.toString()
            })
        }
        return formJSON
    }
    function send_form(url, data) {
        $.ajax({
            url:     url, 
            type:     "POST", 
            dataType: "json", 
            data: data,  
            success: function(response) { 
                $('.alert-success').addClass('active')
                console.log(response)
            },
            error: function(error) { 
                $('.alert-danger').addClass('active')
            }
         });
    }
})